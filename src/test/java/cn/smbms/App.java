package cn.smbms;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import cn.smbms.dao.user.UserMapper;
import cn.smbms.pojo.User;

/**
 * 使用MyBatis(基础)
 * @author teaper
 *
 */
public class App{
	private Logger  logger =Logger.getLogger(App.class);
	
	@Before
	public void setUp() throws Exception{
		
	}
	//使用@Test必须引入junit架包
	//第一种方式(namespace)
	@Test
    public void Test(){
    	String resource="mybatis-config.xml";
    	int count = 0;
    	SqlSession sqlSession = null;
    	try {
    		//1 获取mybatis-config.xml的输入流
			InputStream is = Resources.getResourceAsStream(resource);
			//2 创建SqlSessionFactory对象,完成对配置文件的读取
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
			//3 创建sqlSession
			sqlSession = factory.openSession();
			//4 调用mapper文件的namespace和子元素的id来找到相应SQL并且执行,必须将mapper文件引入到mybatis-config.xml中
			count = sqlSession.selectOne("cn.smbms.dao.user.UserMapper.getCount");
			logger.debug("COUNT等于:---->"+count);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			//关闭sqlSession对象
			sqlSession.close();
		}
    }
	
	@Test
    public void testGetUserList(){
    	String resource="mybatis-config.xml";
    	List<User> userlist= null;
    	SqlSession sqlSession = null;
    	try {
    		//1 获取mybatis-config.xml的输入流
			InputStream is = Resources.getResourceAsStream(resource);
			//2 创建SqlSessionFactory对象,完成对配置文件的读取
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
			//3 创建sqlSession
			sqlSession = factory.openSession();
			//4 调用mapper文件的namespace和子元素的id来找到相应SQL并且执行,必须将mapper文件引入到mybatis-config.xml中
			userlist = sqlSession.selectList("cn.smbms.dao.user.UserMapper.gitUserList");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			//关闭sqlSession对象
			sqlSession.close();
		}
    	for(User user:userlist){
			logger.debug(user.getAddress()+"\t"+user.getUserName());
    	}
    }
	
	//第二种方式(getMapper)
	@Test
    public void testCount(){
    	String resource="mybatis-config.xml";
    	int count = 0;
    	SqlSession sqlSession = null;
    	try {
    		//1 获取mybatis-config.xml的输入流
			InputStream is = Resources.getResourceAsStream(resource);
			//2 创建SqlSessionFactory对象,完成对配置文件的读取
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
			//3 创建sqlSession
			sqlSession = factory.openSession();
			//4 使用getMapper方式,需要先创建UserMapper interface 方法,且interface方法名和mapper中id保持一致
			count = sqlSession.getMapper(UserMapper.class).getCount();
			logger.debug("COUNT等于:---->"+count);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			//关闭sqlSession对象
			sqlSession.close();
		}
    }
}
